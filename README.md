# HUATAN ATTENDANCE

Aplicación movil para el control de asistencia de los empleados

## Features

- Geolocalizacion cada 5 minutos
- Envio de notificaciones si existe un cambio en el lugar de trabajo
- Envio de documentos para justificantes medicos
- Enviar la hora entrada y salida
- Solicitud de vacaciones
- Validación del dispositivo mediante OTP
