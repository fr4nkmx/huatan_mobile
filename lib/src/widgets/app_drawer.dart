import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

class AppDrawer extends StatefulWidget {
  const AppDrawer({Key key}) : super(key: key);

  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  String version;
  String buildNumber;

  @override
  void initState() {
    super.initState();
    _getVersion();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _createHeader(),
          _createDrawerItem(
              icon: Icons.home,
              text: 'Inicio',
              onTap: () => Navigator.pushReplacementNamed(context, 'home')),
          _createDrawerItem(
              icon: Icons.access_time,
              text: 'Asistencia',
              onTap: () =>
                  Navigator.pushReplacementNamed(context, 'asistencia')),
          _createDrawerItem(
              icon: Icons.local_hospital,
              text: 'Incapacidades',
              onTap: () =>
                  Navigator.pushReplacementNamed(context, 'incapacidad')),
          _createDrawerItem(
              icon: Icons.beach_access,
              text: 'Vacaciones',
              onTap: () =>
                  Navigator.pushReplacementNamed(context, 'vacaciones')),
          _createDrawerItem(
              icon: Icons.swap_vert,
              text: 'Cambio de ubicación',
              onTap: () => Navigator.pushReplacementNamed(context, 'cambios')),
          _createDrawerItem(
              icon: Icons.gps_fixed,
              text: 'Actualizar regiones',
              onTap: () => _updateRegions()),
          ListTile(
            title: Text('$version'),
            onTap: () {},
          ),
        ],
      ),
    );
  }

  Widget _createHeader() {
    return DrawerHeader(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.symmetric(horizontal: 15.0),
        decoration: BoxDecoration(
          color: Color.fromRGBO(121, 159, 146, 1.0),
          image: DecorationImage(
            image: AssetImage('assets/huatan_white.png'),
          ),
        ),
        child: Stack(children: <Widget>[
          Positioned(
              bottom: 12.0,
              left: 16.0,
              child: Text('',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w500))),
        ]));
  }

  Widget _createDrawerItem(
      {IconData icon, String text, GestureTapCallback onTap}) {
    return ListTile(
      title: Row(
        children: <Widget>[
          Icon(icon),
          Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: Text(text),
          )
        ],
      ),
      onTap: onTap,
    );
  }

  _updateRegions() {
    print('UPDATE REGIONES GPS');
  }

  _getVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    version = packageInfo.version;
    buildNumber = packageInfo.buildNumber;

    setState(() {
      version = packageInfo.version;
      buildNumber = packageInfo.buildNumber;
    });
  }
}
