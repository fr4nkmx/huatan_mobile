import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:attendance/src/bloc/otp_validator.dart';

class OtpBloc with OtpValidator {
  final _telefonoController = BehaviorSubject<String>();
  final _codigoController = BehaviorSubject<String>();
  final _isLoadingController = BehaviorSubject<bool>();
  final _isErrorController = BehaviorSubject<bool>();
  final _isCorrectController = BehaviorSubject<bool>();

  // Recupera datos, solo si transform se cumple regresa el valor
  Stream<String> get telefonoStream =>
      _telefonoController.stream.transform(validarTelefono);

  Stream<String> get codigoStream =>
      _codigoController.stream.transform(validarCodigo);

  Stream<bool> get isLoadingStream => _isLoadingController.stream;
  Stream<bool> get isErrorStream => _isErrorController.stream;

  Stream<bool> get isCorrectStream => _isCorrectController.stream;

  // Escribe datos
  Function(String) get changeTelefono => _telefonoController.sink.add;
  Function(String) get changeCodigo => _codigoController.sink.add;

  Function(bool) get changeIsLoading => _isLoadingController.sink.add;
  Function(bool) get changeIsError => _isErrorController.sink.add;
  Function(bool) get changeIsCorrect => _isCorrectController.sink.add;

  String get telefono => _telefonoController.value;
  String get codigo => _codigoController.value;

  bool get isLoading => _isLoadingController.value;
  bool get isError => _isErrorController.value;
  bool get isCorrect => _isCorrectController.value;

  dispose() {
    _codigoController?.close();
    _telefonoController?.close();
    _isErrorController?.close();
    _isLoadingController?.close();
    _isCorrectController?.close();
  }
}
