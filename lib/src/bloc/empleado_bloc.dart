import 'package:attendance/src/models/empleado_model.dart';
import 'package:attendance/src/provider/empleado_provider.dart';
import 'package:attendance/src/services/geofence_service.dart';
import 'package:attendance/src/utils/preferences.dart';
import 'package:rxdart/rxdart.dart';

class EmpleadoBloc {
  final _empleadoController = BehaviorSubject<EmpleadoModel>();
  final _cargandoController = BehaviorSubject<bool>();
  final _empleadoProvider = EmpleadoProvider();
  final _prefs = Preferences();
  final _geofenceService = GeofenceService();

  // Cuando se incializa la aplicacion se llama la primera vez
  EmpleadoBloc() {
    // INICIALIZAMOS LOS DATOS EN EL BLOC SI YA SE ENCUENTRA REGISTRADO
    if (_prefs.empleado != null) {
      getData();
    }
  }

  getData() async {
    final nomina = _prefs.empleado.numNomina;
    final response = await _empleadoProvider.getData(nomina);
    print('### RESPONSE EMPLEADO ###');
    print(response);

    if (response.statusCode == 200) {
      _prefs.empleado = response.result;
      _empleadoController.sink.add(response.result);
    } else {
      _empleadoController.sink.addError(response.statusCode);
    }
  }

  updateToken() async {}

  register(
    String telefono,
    String firebaseId,
    String fcmToken,
    String deviceId,
  ) async {
    final res = await _empleadoProvider.register(
        telefono, firebaseId, fcmToken, deviceId);

    switch (res.statusCode) {
      case 200:
        if (res.result.tramoModel != null) {
          _prefs.empleado = res.result;
          _prefs.resEmpleado = 200;
          _empleadoController.sink.add(res.result);
          
          // registramos las geocercas
          await _geofenceService.registerGeofences(
            res.result.geocercaModel            
          );

        } else {
          _prefs.resEmpleado = 404;
          _prefs.empleado = null;
        }
        break;
      case 404:
        _prefs.resEmpleado = 404;
        _prefs.empleado = null;
        break;
      case 500:
        _prefs.resEmpleado = 500;
        _prefs.empleado = null;
        break;
      default:
        _prefs.resEmpleado = null;
        _prefs.empleado = null;
    }
  }

  // NOT IMPLEMENTADO
  update(
    String telefono,
    String firebaseId,
    String fcmToken,
    String deviceId,
  ) async {
    final res = await _empleadoProvider.register(
        telefono, firebaseId, fcmToken, deviceId);

    switch (res.statusCode) {
      case 200:
        if (res.result.tramoModel != null) {
          _prefs.empleado = res.result;
          _prefs.resEmpleado = 200;
          _empleadoController.sink.add(res.result);

          await _geofenceService.registerGeofence(
            res.result.obra,
            res.result.tramoModel.lat,
            res.result.tramoModel.lon,
            200.0,
          );

        } else {
          _prefs.resEmpleado = 404;
          _prefs.empleado = null;
        }
        break;
      case 404:
        _prefs.resEmpleado = 404;
        _prefs.empleado = null;
        break;
      case 500:
        _prefs.resEmpleado = 500;
        _prefs.empleado = null;
        break;
      default:
        _prefs.resEmpleado = null;
        _prefs.empleado = null;
    }
  }

  Stream<EmpleadoModel> get empleadoStream => _empleadoController.stream;

  dispose() {
    _empleadoController?.close();
    _cargandoController?.close();
  }
}
