import 'package:attendance/src/models/vacacion_model.dart';
import 'package:attendance/src/provider/vacacion_provider.dart';
import 'package:attendance/src/utils/preferences.dart';
import 'package:rxdart/rxdart.dart';

class VacacionBloc {
  final _vacacionController = BehaviorSubject<VacacionModel>();
  final _provider = VacacionProvider();

  final _prefs = Preferences();

  get() async {
    final vacacion = await _provider.get();

    if (vacacion.statusCode == 200) {
      _prefs.vacacion = vacacion.result;
      _vacacionController.sink.add(vacacion.result);
    } else {
      _prefs.vacacion = null;
      _vacacionController.sink.addError(vacacion.statusCode);
    }
  }

  add(DateTime ini, DateTime fin) async {
    final vacacion = await _provider.add(ini, fin);
    if (vacacion.statusCode == 200) {
      _prefs.vacacion = vacacion.result;
      _vacacionController.sink.add(vacacion.result);
    } else {
      _prefs.vacacion = null;
      _vacacionController.sink.addError(vacacion.statusCode);
    }
  }

  Stream<VacacionModel> get vacacionStream => _vacacionController.stream;

  dispose() {
    _vacacionController?.close();
  }
}
