import 'dart:isolate';
import 'dart:ui';

import 'package:attendance/src/bloc/asistencia_bloc.dart';
import 'package:attendance/src/services/local_notification_service.dart';
import 'package:rxdart/rxdart.dart';

class GeofenceBloc {
  final _geofenceEventController = BehaviorSubject<String>();
  final _geofenceReadyController = BehaviorSubject<bool>();
  final _notificacion = LocalNotificationService();
  final _asistenciaBloc = AsistenciaBloc();

  GeofenceBloc() {
    print('Inicializando los datos en el bloc');
    listenGeofence();
  }

  // todo: AL MENOS UNO VERDADERO

  listenGeofence() {
    final _port = ReceivePort();
    final existsPort = IsolateNameServer.registerPortWithName(
        _port.sendPort, 'geofencing_send_port');

    final title = 'GEOCERCA TRAMO ASIGNADO';

    if (existsPort) {
      _port.listen((dynamic data) {
        print('##### LISTENGEOFENCE #####');
        print(data);
        _geofenceEventController.sink.add(data);

        switch (data) {
          case 'GeofenceEvent.enter':
            print('### ENTRO A GEOCERCA ###');
            _asistenciaBloc.enableEntrada(true);
            _notificacion.show(title, 'Ha entrado a la zona de trabajo');
            break;
          case 'GeofenceEvent.dwell':
            print('### SIGO EN GEOCERCA ###');
            _asistenciaBloc.enableEntrada(true);
            _notificacion.show(title, 'Ha entrado a la zona de trabajo');
            break;
          case 'GeofenceEvent.exit':
            print('### SALGO DE GEOCERCA ###');
            _asistenciaBloc.enableEntrada(false);
            _notificacion.show(title, 'Ha salido de la zona de trabajo');
            break;
        }
      });
    }
  }

  // Obtiene los eventos de la geocerca
  Stream<String> get geofenceEventStream => _geofenceEventController.stream;

  // Indica si la geocerca esta lista
  Stream<bool> get geofenceReady => _geofenceReadyController.stream;

  dispose() {
    _geofenceEventController?.close();
    _geofenceReadyController?.close();
  }
}
