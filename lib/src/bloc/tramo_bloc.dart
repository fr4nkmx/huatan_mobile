import 'package:attendance/src/utils/preferences.dart';
import 'package:rxdart/rxdart.dart';

class TramoBloc {
  final _tramoController = BehaviorSubject<String>();

  final _prefs = Preferences();

  TramoBloc() {
    print('### INITI TRAMO BLOC ###');
    refresh();
  }

  refresh() {
    if (_prefs?.empleado != null) {
      _tramoController.sink.add(_prefs.empleado.tramoModel.nombre);
    }
  }

  Stream<String> get tramoStream => _tramoController.stream;

  dispose() {
    _tramoController?.close();
  }
}
