import 'package:attendance/src/models/cambio_tramo_model.dart';
import 'package:attendance/src/provider/cambio_tramo_provider.dart';
import 'package:attendance/src/utils/preferences.dart';

import 'package:rxdart/rxdart.dart';

class CambioTramoBloc {
  final _cambioTramoController = BehaviorSubject<CambioTramoModel>();
  final _loadingController = BehaviorSubject<bool>();
  final _provider = CambioTramoProvider();
  final _prefs = Preferences();

  get() async {
    final cambioTramo = await _provider.get();

    if (cambioTramo.statusCode == 200) {
      _prefs.cambioTramo = cambioTramo.result;
      _cambioTramoController.sink.add(cambioTramo.result);
    } else {
      _prefs.cambioTramo = null;
      _cambioTramoController.sink.addError(cambioTramo.statusCode);
    }
  }

  update(int status) async {
    final cambioTramo = await _provider.update(status);
    if (cambioTramo.statusCode == 201) {
      _prefs.cambioTramo = cambioTramo.result;
      _cambioTramoController.sink.add(cambioTramo.result);
    } else {
      _prefs.cambioTramo = null;
      _cambioTramoController.sink.addError(cambioTramo.statusCode);
    }
  }

  Stream<CambioTramoModel> get cambioTramoStream =>
      _cambioTramoController.stream;

  dispose() {
    _cambioTramoController?.close();
    _loadingController?.close();
  }
}
