import 'dart:async';

class OtpValidator {
  final validarCodigo = StreamTransformer<String, String>.fromHandlers(
      handleData: (codigo, sink) {
    if (codigo.length == 6) {
      sink.add(codigo);
    } else {
      sink.addError('Deben ser 6 caracteres');
    }
  });

  final validarTelefono = StreamTransformer<String, String>.fromHandlers(
      handleData: (telefono, sink) {
    final pattern = r'(^[0-9]{10}$)';

    final regExp = new RegExp(pattern);

    if (telefono.length >= 10) {
      if (regExp.hasMatch(telefono)) {
        sink.add(telefono);
      } else {
        sink.add(null);
        sink.addError('Ingresar un numero válido');
      }
    } else {
      sink.add(null);
    }
  });
}
