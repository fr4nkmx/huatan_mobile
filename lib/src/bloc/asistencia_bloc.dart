import 'package:attendance/src/models/asistencia_mes.dart';
import 'package:attendance/src/models/asistencia_model.dart';
import 'package:attendance/src/models/asistencia_semana.dart';
import 'package:attendance/src/models/asistencia_state_model.dart';
import 'package:attendance/src/provider/asistencia_provider.dart';
import 'package:attendance/src/services/location_service.dart';
import 'package:attendance/src/utils/preferences.dart';
import 'package:jiffy/jiffy.dart';
import 'package:rxdart/rxdart.dart';

class AsistenciaBloc {
  final _prefs = Preferences();
  final _provider = AsistenciaProvider();
  final _location = LocationService();

  final _asistenciaController = BehaviorSubject<AsistenciaModel>();
  final _asistenciaStateController = BehaviorSubject<AsistenciaStateModel>();

  final _resumenSemanaController = BehaviorSubject<List<AsistenciaSemana>>();
  final _resumenMesController = BehaviorSubject<AsistenciaMes>();

  AsistenciaStateModel _state = new AsistenciaStateModel();

  init() {
    // limpiamos el estado para el nuevo día

    //  PUEDE HABER NULOS, SOLO IMPORTA HOY.
    //  SOLO SE DESHABILITA POR GEOCERCA
    // [SUNDAY -> 0,  MON -> 1, SAT -> 6 ]
        
    // todo: CHECK PREFS
    if (_prefs.asistenciaState != null && _prefs.asistenciaState.dia != null) {
      // NUEVO DIA
      if (_prefs.asistenciaState.dayOfYear < Jiffy().dayOfYear) {
        print('### es un nuevo dia ###');
        print('## ELIMINAR DATOS ###');
        _prefs.asistenciaState = null;
      }
    }

    _state = _prefs.asistenciaState != null
        ? _prefs.asistenciaState
        : new AsistenciaStateModel();
    _asistenciaStateController.sink.add(_state);
  }

  enableEntrada(bool value) {
    if (_prefs.asistenciaState == null ||
        _prefs.asistenciaState.horaEntrada == '') {
      print('### HABILITANDO BOTON PARA REGISTRAR ENTRADA ###');
      _state.entradaEnable = value;
      _asistenciaStateController.sink.add(_state);
    }
  }

  // enableSalida(bool value) {
  //   print('### HABILITANDO BOTON PARA REGISTRAR ENTRADA ###');
  //   _state.entradaEnable = false;
  //   _state.salidaEnable = true;
  //   _asistenciaStateController.sink.add(_state);
  // }

  // Envia la hora de entrada
  checkin() async {
    _asistenciaStateController.sink.add(null);

    await _provider.entrada();
    // ES NECESARIO?
    _asistenciaController.sink.add(_prefs.asistencia);
    _location.start();

    _state.terminaJornada = false;
    _state.entradaEnable = false;
    _state.salidaEnable = true;
    _state.horaEntrada = _prefs.asistencia.horaEntrada;
    _state.dia = Jiffy().day;
    _state.week = Jiffy().week;
    _state.dayOfYear = Jiffy().dayOfYear;
    _prefs.asistenciaState = _state;
    _asistenciaStateController.sink.add(_state);
  }

  // Envia la hora de salida
  checkout() async {
    _asistenciaStateController.sink.add(null);

    await _provider.salida();
    _asistenciaController.sink.add(_prefs.asistencia);

    _location.stop();

    _state.terminaJornada = true;
    _state.entradaEnable = false;
    _state.salidaEnable = false;
    _state.horaSalida = _prefs.asistencia.horaSalida;
    _state.dia = Jiffy().day;
    _state.week = Jiffy().week;
    _state.dayOfYear = _state.dayOfYear;
    // hay que llenar semana

    // Obtenemos el dia de la semana

    // mapeamos el tipo de "bool"

    // sumar en mes dependiendo del tipo de "bool"

    // resetear por mes

    _prefs.asistenciaState = _state;
    _asistenciaStateController.sink.add(_state);
  }

  resumenMes() async {
    // _resumenMesController.sink.add(null);
    await _provider.resumenMes();
    _resumenMesController.sink.add(_prefs.asistenciaMes);
  }

  resumenSemana() async {
    // _resumenSemanaController.sink.add(null);
    await _provider.resumenSemana();

    _resumenSemanaController.sink.add(semanaCompleta(_prefs.asistenciaSemana));
  }

  // reinicia el estado de la aplicacion
  deafultState() {
    _state.entradaEnable = false;
    _state.salidaEnable = false;
    _state.horaEntrada = '';
    _state.horaSalida = '';
    _state.terminaJornada = false;
    _asistenciaStateController.sink.add(_state);
  }

  List<AsistenciaSemana> semanaCompleta(List<AsistenciaSemana> newData) {
    var result = new List<AsistenciaSemana>();

    var dias = [
      'DOMINGO',
      'LUNES',
      'MARTES',
      'MIERCOLES',
      'JUEVES',
      'VIERNES',
      'SABADO',
    ];

    dias.asMap().forEach((index, value) {
      var tmp = newData.firstWhere((d) => d.index == index, orElse: () => null);

      if (tmp != null) {
        result.add(
          AsistenciaSemana(
              index: tmp.index,
              diaSemana: dias[index],
              horaEntrada: tmp.horaEntrada,
              horaSalida: tmp.horaSalida,
              asistencia: tmp.asistencia,
              retardo: tmp.retardo,
              falta: tmp.falta),
        );
      } else {
        result.add(
          AsistenciaSemana(
              index: index,
              diaSemana: dias[index],
              horaEntrada: '',
              horaSalida: '',
              asistencia: false,
              retardo: false,
              falta: false),
        );
      }
    });

    return result;
  }

  // ESTO ES PARA TEMAS DE PRUEBA

  Stream<AsistenciaModel> get asistenciaStream => _asistenciaController.stream;

  Stream<AsistenciaStateModel> get asistenciaStateStream =>
      _asistenciaStateController.stream;

  Stream<List<AsistenciaSemana>> get resumenSemanaStream =>
      _resumenSemanaController.stream;

  Stream<AsistenciaMes> get resumenMesStream => _resumenMesController.stream;

  dispose() {
    _asistenciaController?.close();
    _asistenciaStateController?.close();

    _resumenSemanaController?.close();
    _resumenMesController?.close();
  }
}
