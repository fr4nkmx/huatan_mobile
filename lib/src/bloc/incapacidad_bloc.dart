import 'package:attendance/src/models/incapacidad_model.dart';
import 'package:attendance/src/provider/incapacidad_provider.dart';
import 'package:rxdart/rxdart.dart';

class IncapacidadBloc {
  final _provider = IncapacidadProvider();

  final _incapacidadesController = BehaviorSubject<List<IncapacidadModel>>();

  Stream<List<IncapacidadModel>> get incapacidadesStream =>
      _incapacidadesController.stream;

  getAll() async {
    final incapacidades = await _provider.getByEmpleado();
    _incapacidadesController.sink.add(incapacidades);
  }

  dispose() {
    _incapacidadesController?.close();
  }
}
