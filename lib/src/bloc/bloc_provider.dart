import 'package:attendance/src/bloc/asistencia_bloc.dart';
import 'package:attendance/src/bloc/cambio_tramo_bloc.dart';
import 'package:attendance/src/bloc/empleado_bloc.dart';
import 'package:attendance/src/bloc/geofence_bloc.dart';
import 'package:attendance/src/bloc/incapacidad_bloc.dart';
import 'package:attendance/src/bloc/tramo_bloc.dart';
import 'package:attendance/src/bloc/vacacion_bloc.dart';

export 'package:attendance/src/bloc/otp_bloc.dart';
export 'package:attendance/src/bloc/geofence_bloc.dart';

import 'package:flutter/material.dart';

class BlocProvider extends InheritedWidget {
  final _empleadoBloc = EmpleadoBloc();
  final _asistenciaBloc = AsistenciaBloc();

  final _tramoBloc = TramoBloc();
  final _geofenceBloc = GeofenceBloc();
  final _incapacidadBloc = IncapacidadBloc();
  final _cambioTramoBloc = CambioTramoBloc();
  final _vacacionBloc = VacacionBloc();
  //  INICIA Implementation for InheritedWidget
  static BlocProvider _instancia;

  factory BlocProvider({Key key, Widget child}) {
    if (_instancia == null) {
      _instancia = new BlocProvider._internal(key: key, child: child);
    }

    return _instancia;
  }

  BlocProvider._internal({Key key, Widget child})
      : super(key: key, child: child) {
    print('CONSTRUCTOR DEL INHERIT WIDGET');
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }

  // TERMINA Implementation for InheritedWidget

  static EmpleadoBloc empleadoBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<BlocProvider>()
        ._empleadoBloc;
  }

  static AsistenciaBloc asistenciaBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<BlocProvider>()
        ._asistenciaBloc;
  }

  static TramoBloc tramoBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<BlocProvider>()
        ._tramoBloc;
  }

  static GeofenceBloc geofenceBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<BlocProvider>()
        ._geofenceBloc;
  }

  static IncapacidadBloc incapacidadBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<BlocProvider>()
        ._incapacidadBloc;
  }

  static CambioTramoBloc cambioTramoBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<BlocProvider>()
        ._cambioTramoBloc;
  }

  static VacacionBloc vacacionBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<BlocProvider>()
        ._vacacionBloc;
  }
}
