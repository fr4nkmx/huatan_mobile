import 'dart:io';
import 'package:attendance/src/models/incapacidad_model.dart';
import 'package:dio/adapter.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:jiffy/jiffy.dart';
import 'package:path/path.dart';

import 'package:attendance/src/data/env.dart' as env;

import 'package:attendance/src/utils/preferences.dart';
import 'package:dio/dio.dart';

class IncapacidadProvider {
  final String _url = "${env.apiUrl}/incapacidades";
  final _prefs = Preferences();
  final _dio = new Dio();

  IncapacidadProvider() {
    (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      print('onHttpClientCreate entered...'); // this code is never reached
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
  }

  Future<dynamic> add(File image, String motivo) async {
    try {
      Response response;

      var resized = await FlutterImageCompress.compressAndGetFile(
        image.absolute.path,
        image.absolute.path,
        quality: 85,
        minHeight: 1920,
        minWidth: 1080,
        format: CompressFormat.png,
        keepExif: false,
      );

      final filename = basename(resized.path);
      final fecha = Jiffy().format("dd-MM-yyyy");
      print('FECHA: $fecha');

      FormData formData = new FormData.fromMap({
        "empleadoId": _prefs.empleado.id,
        "motivo": motivo,
        "file": await MultipartFile.fromFile(resized.path, filename: filename),
        "fecha": fecha,
        "estado": 0
      });

      response = await _dio.post(_url, data: formData);
      print('#### RESPONSE ####');
      print(response);
      if (response.statusCode != 201) {
        print('NO SE REGISTRO LA INCAPACIDAD');
        return null;
      } else {
        print('### RESPONSE EXTRA ###');
        print(response.data);
        return response.data;
      }
    } catch (e) {
      print('############## ERROR #############');
      print('############## $e #############');
      return null;
    }
  }

  Future<List<IncapacidadModel>> getByEmpleado() async {
    try {
      Response response;
      final _urlEmpleado = '$_url/empleado/${_prefs.empleado.id}';

      print('### URL TO CONSULT ###');
      print(_urlEmpleado);

      response = await _dio.get(_urlEmpleado);
      if (response.statusCode == 200) {
        print('### RESPONSE FROM SERVER ###');
        print(response.data);
        // var list = json.decode(response.data) as List;
        List<IncapacidadModel> result = List<IncapacidadModel>.from(
            response.data.map((model) => IncapacidadModel.fromJson(model)));
        // list.map((i) => IncapacidadModel.fromJson(i)).toList();
        return result;
      } else {
        print('### SIN RESULTADOS ###');
        return null;
      }
    } catch (e) {
      print('### OCURRIO UN ERROR ###');
      return null;
    }
  }
}
