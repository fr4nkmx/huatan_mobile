import 'dart:convert';
import 'dart:io';

import 'package:attendance/src/data/env.dart' as env;
import 'package:attendance/src/models/dto/location_dto.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';

class LocationProvider {
  final String _url = "${env.apiUrl}/locations";
  final _dio = new Dio();

  LocationProvider() {
    (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      print('onHttpClientCreate entered...');
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
  }

  Future<dynamic> add(LocationDto data) async {
    try {
      Response response;

      var req = jsonEncode(data);
      response = await _dio.post(_url, data: req);

      if (response.statusCode == 200) {
        return true;
      } else
        return false;
    } on DioError catch (e) {
      return false;
    }
  }
}
