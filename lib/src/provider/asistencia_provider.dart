import 'dart:convert';
import 'dart:io';

import 'package:attendance/src/models/asistencia_mes.dart';
import 'package:attendance/src/models/asistencia_model.dart';
import 'package:attendance/src/models/asistencia_semana.dart';

import 'package:attendance/src/utils/preferences.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/io_client.dart';

import 'package:attendance/src/data/env.dart' as env;
import 'package:jiffy/jiffy.dart';

/*
  Clase para el envio de informacion al backend .net, cambiar por dio
*/
class AsistenciaProvider {
  final String _url = "${env.apiUrl}/asistencias";
  final _prefs = Preferences();

  Future<void> resumenSemana() async {
    try {
      final data = json.encode(
        {
          'empleadoId': '${_prefs.empleado.id}',
          'semana': '${Jiffy().week}',
          'anio': '${Jiffy().year}',
          'mes': '0'
        },
      );

      final http = _getHttpClient();

      print('$_url/resumen/semana');

      final res = await http.post(
        '$_url/resumen/semana',
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
        },
        body: data,
      );

      // print('### RESPONSE ####');

      // mandamos la info al stream?
      if (res.statusCode == 404) {
        print('No se encontro');
        return null;
      } else {
        // print(res.body);
        print('### RESPONSE FROM RESUMEN SEMANA ###');
        print(res.body);

        final asistenciaSemana = asistenciaSemanaFromJson(res.body);
        _prefs.asistenciaSemana = asistenciaSemana;
      }
    } catch (e) {
      print('############## ERROR #############');
      print('############## $e #############');
      throw Exception("Error al consultar el servidor");
    }
  }

  Future<void> resumenMes() async {
    try {
      final body = json.encode(
        {
          'empleadoId': _prefs.empleado.id,
          'mes': Jiffy().month,
          'anio': Jiffy().year,
        },
      );

      final http = _getHttpClient();
      final res = await http.post(
        '$_url/resumen/mes',
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
        },
        body: body,
      );

      // print('### RESPONSE ####');

      // mandamos la info al stream?
      if (res.statusCode == 404) {
        print('No se encontro');
        return null;
      } else {
        // print(res.body);
        print('### RESPONSE FROM RESUMEN MES ###');
        print(res.body);

        final asistenciaMes = asistenciaMesFromJson(res.body);
        _prefs.asistenciaMes = asistenciaMes;
      }
    } catch (e) {
      print('############## ERROR #############');
      print('############## $e #############');
      throw Exception("Error al consultar el servidor");
    }
  }

  Future<void> entrada() async {
    try {
      final http = _getHttpClient();

      final position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

      final data = json.encode({
        'numNomina': _prefs.empleado.numNomina,
        'deviceId': _prefs.empleado.deviceId,
        'lat': position.latitude,
        'lon': position.longitude,
        'semanaAnio': Jiffy().week
      });

      final res = await http.post('$_url',
          headers: {
            "Accept": "application/json",
            "content-type": "application/json",
          },
          body: data);

      if (res.statusCode == 201) {
        print('### RESPONSE FROM ASISTENCIAS ###');
        print(res.body);

        final asistencia = asistenciaModelFromJson(res.body);
        _prefs.asistencia = asistencia;
      } else {        
        print('Ocurrio un error al registrar la entrada');
        return null; // print(res.body);
      }
    } catch (e) {
      print('############## ERROR #############');
      print('############## $e #############');
      throw Exception("Error al consultar el servidor");
    }
  }

  Future<void> salida() async {
    try {
      final http = _getHttpClient();
      final entradaId = _prefs.asistencia.asistenciaId;

      final position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

      final data = json.encode({
        'empleadoId': _prefs.empleado.id,
        'deviceId': _prefs.empleado.deviceId,
        'lat': position.latitude,
        'lon': position.longitude,
      });

      final res = await http.put('$_url/$entradaId',
          headers: {
            "Accept": "application/json",
            "content-type": "application/json",
          },
          body: data);

      if (res.statusCode == 200) {
        print('### RESPONSE FROM ASISTENCIAS ###');
        print(res.body);
        final asistencia = asistenciaModelFromJson(res.body);
        _prefs.asistencia = asistencia;
      } else {        
        print('Ocurrio un error al registrar la salida');
        return null;
      }
    } catch (e) {
      print('############## ERROR #############');
      print('############## $e #############');
      throw Exception("Error al consultar el servidor");
    }
  }

  IOClient _getHttpClient() {
    final ioc = HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    return IOClient(ioc);
  }
}
