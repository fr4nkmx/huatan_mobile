import 'dart:io';

import 'package:attendance/src/models/response/empleado_response.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';

import 'package:attendance/src/data/env.dart' as env;

class EmpleadoProvider {
  final String _url = "${env.apiUrl}/empleados";
  final _dio = Dio();

  EmpleadoProvider() {
    (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      print('onHttpClientCreate entered...'); // this code is never reached
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
  }

  Future<EmpleadoResponse> getData(String nomina) async {
    Response res;
    try {
      final _endpoint = '$_url/mobile/$nomina';
      print('### ENDPOINT ###');
      print(_endpoint);
      res = await _dio.get(_endpoint);
      return EmpleadoResponse.fromJson(res.data, res.statusCode);
    } on DioError catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return EmpleadoResponse.withError("$error");
    }
  }

  /// ENVIA LA INFORMACION DEL DISPOSITIVO PARA REGISTRAR

  Future<EmpleadoResponse> register(
    String telefono,
    String firebaseId,
    String fcmToken,
    String deviceId,
  ) async {
    Response res;
    try {
      res = await _dio.post('$_url/dispositivo/registra', data: {
        'telefono': telefono,
        'firebaseId': firebaseId,
        'fcmToken': fcmToken,
        'deviceId': deviceId,
      });

      return EmpleadoResponse.fromJson(res.data, res.statusCode);
    } on DioError catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return EmpleadoResponse.withError("$error");
    }
  }

  Future<Response> updateToken(int empleadoId, String fcmToken) async {
    Response res = await _dio.post('$_url/dispositivo/update_token', data: {
      'empleadoId': empleadoId,
      'fcmToken': fcmToken,
    });

    return res;
  }
}
