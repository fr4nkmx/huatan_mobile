import 'dart:io';
import 'package:attendance/src/models/response/cambio_tramo_response.dart';
import 'package:dio/adapter.dart';

import 'package:attendance/src/data/env.dart' as env;

import 'package:attendance/src/utils/preferences.dart';
import 'package:dio/dio.dart';

class CambioTramoProvider {
  final String _url = "${env.apiUrl}/cambiotramos";
  final _prefs = Preferences();
  final _dio = Dio();

  CambioTramoProvider() {
    (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      print('onHttpClientCreate entered...'); // this code is never reached
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
  }

  Future<CambioTramoResponse> update(status) async {
    Response res;
    try {
      final endpoint = '$_url/${_prefs.cambioTramo.id}';
      res = await _dio.put(endpoint, data: {"estadoNotificacion": status});

      return CambioTramoResponse.fromJson(res.data, res.statusCode);
    } on DioError catch (error, stacktrace) {
      print('############## EXCEPTION CAMBIOTRAMO PROVIDER #############');
      print("Exception occured: $error stackTrace: $stacktrace");
      return resolveError(error, stacktrace);
    }
  }

  Future<CambioTramoResponse> get() async {
    Response res;
    try {
      res = await _dio.get('$_url/empleado/${_prefs.empleado.id}');

      return CambioTramoResponse.fromJson(res.data, res.statusCode);
    } on DioError catch (error, stacktrace) {
      return resolveError(error, stacktrace);
    }
  }

  CambioTramoResponse resolveError(DioError error, StackTrace stacktrace) {
    String errorDescription;
    int statusCode;

    print('############## EXCEPTION CAMBIOTRAMO PROVIDER #############');
    print("Exception occured: $error stackTrace: $stacktrace");

    switch (error.type) {
      case DioErrorType.CANCEL:
        errorDescription = "Request to API server was cancelled";
        statusCode = error.response.statusCode;
        break;
      case DioErrorType.CONNECT_TIMEOUT:
        errorDescription = "Connection timeout with API server";
        statusCode = error.response.statusCode;
        break;
      case DioErrorType.DEFAULT:
        if (error.error is HttpException) {
          statusCode = 0;
          errorDescription = 'Server down';
        } // server down
        else {
          statusCode = 101;
          errorDescription = 'No internet Connection';
        }

        break;
      case DioErrorType.RECEIVE_TIMEOUT:
        errorDescription = "Receive timeout in connection with API server";
        statusCode = error.response.statusCode;
        break;
      case DioErrorType.RESPONSE:
        errorDescription = error.response.statusMessage;
        statusCode = error.response.statusCode;
        break;
      case DioErrorType.SEND_TIMEOUT:
        errorDescription = "Send timeout in connection with API server";
        statusCode = error.response.statusCode;
        break;
    }
    return CambioTramoResponse.withError(errorDescription, statusCode);
  }
}
