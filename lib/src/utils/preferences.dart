import 'package:attendance/src/models/asistencia_mes.dart';
import 'package:attendance/src/models/asistencia_model.dart';
import 'package:attendance/src/models/asistencia_semana.dart';
import 'package:attendance/src/models/asistencia_state_model.dart';
import 'package:attendance/src/models/cambio_tramo_model.dart';

import 'package:attendance/src/models/empleado_model.dart';
import 'package:attendance/src/models/vacacion_model.dart';

import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static final Preferences _instancia = new Preferences._internal();

  factory Preferences() {
    return _instancia;
  }

  Preferences._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  clear() {
    _prefs.clear();
  }

  EmpleadoModel get empleado {
    final tmp = _prefs.getString('empleado');
    return tmp != null ? empleadoModelFromJson(tmp) : null;
  }

  set empleado(EmpleadoModel model) {
    final value = (model != null) ? empleadoModelToJson(model) : null;
    _prefs.setString('empleado', value);
  }

  AsistenciaModel get asistencia {
    final tmp = _prefs.getString('asistencia');
    return tmp != null ? asistenciaModelFromJson(tmp) : null;
  }

  set asistencia(AsistenciaModel model) {
    final value = (model != null) ? asistenciaModelToJson(model) : null;
    _prefs.setString('asistencia', value);
  }

  AsistenciaStateModel get asistenciaState {
    final tmp = _prefs.getString('asistenciaState');
    return tmp != null ? asistenciaStateModelFromJson(tmp) : null;
  }

  set asistenciaState(AsistenciaStateModel model) {
    final value = (model != null) ? asistenciaStateModelToJson(model) : null;
    _prefs.setString('asistenciaState', value);
  }

  List<AsistenciaSemana> get asistenciaSemana {
    final tmp = _prefs.getString('asistenciaSemana');
    return tmp != null ? asistenciaSemanaFromJson(tmp) : null;
  }

  set asistenciaSemana(List<AsistenciaSemana> list) {
    final value = (list != null) ? asistenciaSemanaToJson(list) : null;
    _prefs.setString('asistenciaSemana', value);
  }

  AsistenciaMes get asistenciaMes {
    final tmp = _prefs.getString('asistenciaMes');
    return tmp != null ? asistenciaMesFromJson(tmp) : null;
  }

  set asistenciaMes(AsistenciaMes model) {
    final value = (model != null) ? asistenciaMesToJson(model) : null;
    _prefs.setString('asistenciaMes', value);
  }

  // Mantiene el estado de la aplicacion en caso de un reinicio
  bool get tracking {
    return _prefs.getBool('tracking');
  }

  set tracking(bool value) {
    _prefs.setBool('tracking', value);
  }

  int get resEmpleado {
    return _prefs.getInt('resEmpleado');
  }

  set resEmpleado(int value) {
    _prefs.setInt('resEmpleado', value);
  }

  bool get signed {
    return _prefs.getBool('signed');
  }

  set signed(bool value) {
    _prefs.setBool('signed', value);
  }

  CambioTramoModel get cambioTramo {
    final tmp = _prefs.getString('cambioTramo');
    return tmp != null ? cambioTramoModelFromJson(tmp) : null;
  }

  set cambioTramo(CambioTramoModel model) {
    final value = (model != null) ? cambioTramoModelToJson(model) : null;
    _prefs.setString('cambioTramo', value);
  }

  VacacionModel get vacacion {
    final tmp = _prefs.getString('vacacion');
    return tmp != null ? vacacionModelFromJson(tmp) : null;
  }

  set vacacion(VacacionModel model) {
    final value = (model != null) ? vacacionModelToJson(model) : null;
    _prefs.setString('vacacion', value);
  }
}
