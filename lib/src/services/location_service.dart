import 'dart:async';
import 'package:attendance/src/models/dto/location_dto.dart';
import 'package:attendance/src/models/empleado_model.dart';
import 'package:attendance/src/provider/location_provider.dart';
import 'package:attendance/src/utils/preferences.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info/device_info.dart';
import 'package:geolocator/geolocator.dart';

import 'package:attendance/src/data/env.dart' as env;

class LocationService {
  static final LocationService _timerService = new LocationService._internal();
  final firestore = Firestore.instance;
  final geo = Geoflutterfire();
  final deviceInfo = DeviceInfoPlugin();
  final _prefs = Preferences();

  // usar el nombre del tramo para que en el mapa seleccione solo un segmento
  final _collection = env.collection;

  final _locationProvider = LocationProvider();

  // por default
  final _duration = Duration(minutes: 15);
  EmpleadoModel empleado;

  String _deviceId = '';

  factory LocationService() {
    return _timerService;
  }

  LocationService._internal();

  Timer _timer;

  init() async {
    // comprobar los valores de firebase
    final info = await deviceInfo.androidInfo;
    _deviceId = info.androidId;

    // Si estaba haciendo tracking lo resume
    if (_prefs.tracking != null && _prefs.tracking) {
      start();
    }
  }

  start([Duration duration]) {
    final empleado = _prefs.empleado;

    Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((position) async {
      var location = LocationDto(
          empleadoId: empleado.id,
          nomina: empleado.numNomina,
          nombre: '${empleado.nombre} ${empleado.paterno} ${empleado.materno}',
          obra: empleado.obra,
          puesto: empleado.puesto,
          telefono: empleado.telefono,
          deviceId: _deviceId,
          lat: position.latitude,
          lon: position.longitude,
          timestamp: DateTime.now().millisecondsSinceEpoch,
          zona: empleado.tramoModel.zona);

      await _locationProvider.add(location);
    });

    _prefs.tracking = true;
    duration = (duration == null) ? _duration : duration;

    print(empleado);

    _timer = Timer.periodic(duration, (timer) {
      Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
          .then((position) async {
        var location = LocationDto(
            empleadoId: empleado.id,
            nomina: empleado.numNomina,
            nombre:
                '${empleado.nombre} ${empleado.paterno} ${empleado.materno}',
            obra: empleado.obra,
            puesto: empleado.puesto,
            telefono: empleado.telefono,
            deviceId: _deviceId,
            lat: position.latitude,
            lon: position.longitude,
            timestamp: DateTime.now().millisecondsSinceEpoch,
            zona: empleado.tramoModel.zona);

        await _locationProvider.add(location);
      });
    });
  }

  stop() {
    print('Cancel');
    // todo: eliminar de la coleccion o enviar el metodo para eliminar
    firestore.collection(_collection).document(_deviceId).delete();
    _prefs.tracking = false;
    _timer?.cancel();
  }
}
