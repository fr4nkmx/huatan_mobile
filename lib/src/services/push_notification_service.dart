import 'package:attendance/src/models/notification_model.dart';
import 'package:attendance/src/provider/empleado_provider.dart';
import 'package:attendance/src/services/local_notification_service.dart';
import 'package:attendance/src/utils/preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:rxdart/rxdart.dart';

class PushNotificationService {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final _prefs = Preferences();
  final _localNotification = LocalNotificationService();

  final _mensajesStreamController = BehaviorSubject<NotificationModel>();

  Stream<NotificationModel> get notificaciones =>
      _mensajesStreamController.stream;

  initNotifications() async {
    _firebaseMessaging.requestNotificationPermissions();

    final fcmToken = await _firebaseMessaging.getToken();
    print('=========== FCM Token =========');
    print('El token es $fcmToken');
    print('===============================');

    if (_prefs.empleado != null && _prefs.empleado.fcmToken != fcmToken) {
      final empleado = _prefs.empleado;
      // empleado.fcmToken = '';
      _prefs.empleado = empleado;

      final empleadoProvider = EmpleadoProvider();
      await empleadoProvider.updateToken(_prefs.empleado.id, fcmToken);
      empleado.fcmToken = fcmToken;
      _prefs.empleado = empleado;

      print('=========== Se Actualizo FCM Token =========');
      print('El token es $fcmToken');
      print('===============================');
    }

    _firebaseMessaging.configure(
      onMessage: (info) async {
        print('============ onMessage ============');
        final title = info['notification']['title'];
        _localNotification.show(
          title,
          info['notification']['body'],
        );
        handleNotification(info);
      },
      onResume: (info) async {
        print('============ onResume ============');
        handleNotification(info);
      },
      onLaunch: (info) async {
        print('============ onLaunch ============');
        handleNotification(info);
      },
    );
  }

  handleNotification(dynamic info) {
    // Enviar la notificacion

    print(info);

    dynamic data = info['data'];

    print('### DATA FROM NOTITICATION ###');
    print(data);

    print('### TEST DATA ###');
    print(data['estadoProceso']);

    print(data);

    _mensajesStreamController.sink.add(data);

    //
    switch (data['tipo_notificacion']) {
      case 'cambio':
        print('NOTIFICACION PARA EL CAMBIO DEL TRAMO');
        break;
      case 'vacacion':
        print('NOTIFICACION PARA ACTUALIZAR EL STATUS DE LA VACACION');
        break;
      case 'incapacidad':
        print('NOTIFICACION PARA ACTUALIZAR EL STATUS DE LA INCAPACIDAD');
        break;
      default:
        break;
    }

    // REDIRECCIONAR AL MODULO
  }

  dispose() {
    _mensajesStreamController?.close();
  }
}
