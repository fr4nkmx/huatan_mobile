import 'dart:isolate';
import 'dart:ui';

import 'package:attendance/src/models/empleado_model.dart';
import 'package:geofencing/geofencing.dart';
import 'package:geolocator/geolocator.dart';

class GeofenceService {
  // Singleton implementation

  static final GeofenceService _geofenceService =
      new GeofenceService._internal();

  factory GeofenceService() {
    return _geofenceService;
  }

  GeofenceService._internal();

  // Variables and methods for geofence
  ReceivePort port = ReceivePort();

  final List<GeofenceEvent> _triggers = <GeofenceEvent>[
    GeofenceEvent.enter,
    GeofenceEvent.dwell,
    GeofenceEvent.exit
  ];

  final AndroidGeofencingSettings _androidSettings = AndroidGeofencingSettings(
    initialTrigger: <GeofenceEvent>[
      GeofenceEvent.enter,
      GeofenceEvent.exit,
      GeofenceEvent.dwell
    ],
    loiteringDelay: 30000, // 30 s
    notificationResponsiveness: 5000, // 5s
  );

  // funciones del singleton
  Future<void> initGeofence() async {
    try {
      print('### INICIALIZANDO  GeofencingManager###');
      await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.medium);

      await GeofencingManager.initialize();
      print('### INCIIALIZADA GeofencingManager###');
    } catch (e) {
      print('Error: $e');
      return false;
    }
  }

  Future<void> registerGeofence(
    String tag,
    double latitude,
    double longitude,
    double radius,
  ) async {
    try {
      final pos = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

      print('### POSITION ###');
      print(pos);

      if (latitude != null || longitude != null)
        await GeofencingManager.registerGeofence(
            GeofenceRegion(
              tag,
              latitude,
              longitude,
              radius,
              _triggers,
              androidSettings: _androidSettings,
            ),
            _callback);
    } catch (e) {
      print('### ERROR REGISTER GEOFENCE ###');
      print(e);
    }
  }

  Future<void> registerGeofences(List<GeocercaModel> data) async {
    Future.forEach(data, (fence) async {
      GeofencingManager.registerGeofence(
          GeofenceRegion(
            fence.zona,
            fence.lat,
            fence.lon,
            fence.rad,
            _triggers,
            androidSettings: _androidSettings,
          ),
          _callback);
    });    
  }

  removeGeofence(String tag) {
    try {
      GeofencingManager.removeGeofenceById(tag);
    } catch (e) {
      print('### FAIL TO REMOVE GEOFENCE');
      print(e);
    }
  }

  // todo: AL MENOS ALGUNA GEOCERCA DEBE SER TRUE

  static void _callback(List<String> ids, Location l, GeofenceEvent e) {
    final date = DateTime.now();
    print('### CALLBACK ###');
    final result = '$date Fences: $ids Location $l Event: $e';
    print(result);
    final SendPort send =
        IsolateNameServer.lookupPortByName('geofencing_send_port');
    send?.send(e.toString());
  }
}
