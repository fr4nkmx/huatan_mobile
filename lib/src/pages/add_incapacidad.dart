import 'dart:io';

import 'package:attendance/src/provider/incapacidad_provider.dart';
import 'package:attendance/src/widgets/app_drawer.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';

class AddIncapacidadPage extends StatefulWidget {
  const AddIncapacidadPage({Key key}) : super(key: key);

  @override
  _IncapacidadPageState createState() => _IncapacidadPageState();
}

class _IncapacidadPageState extends State<AddIncapacidadPage> {
  File _photo;
  bool _isDisabled = false;
  ProgressDialog pr;
  String dropdownValue = 'ENFERMEDAD';

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal);
    pr.style(message: 'Enviando información');
    final dao = IncapacidadProvider();

    return Scaffold(
      appBar: AppBar(
        title: Text('INCAPACIDADES'),
        centerTitle: true,
      ),
      resizeToAvoidBottomPadding: false,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Card(
              elevation: 6.0,
              child: Container(
                padding: new EdgeInsets.all(18.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text(
                          'SOLICITAR INCAPACIDAD',
                          style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    DropdownButton<String>(
                      value: dropdownValue,
                      isExpanded: true,
                      iconSize: 24,
                      icon: Icon(Icons.arrow_downward),
                      elevation: 16,
                      hint: Text('MOTIVO DE LA INCAPACIDAD'),
                      underline: Container(
                        height: 2,
                        color: Colors.green,
                      ),
                      onChanged: (String newValue) {
                        setState(() {
                          print(newValue);
                          this.dropdownValue = newValue;
                        });
                      },
                      items: <String>[
                        'ENFERMEDAD',
                        'ACCIDENTE',
                        'PERMISO',
                        'OTRO'
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Center(
                            child: Text(value),
                          ),
                        );
                      }).toList(),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            IconButton(
                              iconSize: 76.0,
                              icon: Icon(Icons.add_photo_alternate),
                              color: Colors.blue,
                              onPressed: _takeImage,
                            ),
                            Text('Seleccionar imagen'),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            IconButton(
                              iconSize: 76.0,
                              icon: Icon(Icons.add_a_photo),
                              color: Colors.blue,
                              onPressed: _takePhoto,
                            ),
                            Text('Tomar fotografía'),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Card(
              elevation: 6.0,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 18.0, horizontal: 18.0),
                child: _photo == null
                    ? Image(
                        height: 300.0,
                        image: AssetImage('assets/no-image.png'),
                        fit: BoxFit.cover,
                      )
                    : Image.file(
                        _photo,
                        fit: BoxFit.contain,
                        height: 300.0,
                      ),
              ),
            ),
            Card(
              elevation: 6.0,
              child: FlatButton(
                onPressed: _isDisabled
                    ? null
                    : () async {
                        pr.show();
                        await dao.add(_photo, dropdownValue).then((res) {
                          pr.hide();
                          Navigator.of(context).popAndPushNamed('incapacidad');
                        });
                      },
                color: Colors.blue,
                child: Text('Enviar'),
              ),
            ),
          ],
        ),
      ),
      drawer: AppDrawer(),
    );
  }

  Future _takeImage() async {
    _processImage(ImageSource.gallery);
  }

  Future _takePhoto() async {
    _processImage(ImageSource.camera, 85);
  }

  Future _processImage(ImageSource source, [int quality]) async {
    File photo;

    if (quality != null)
      photo =
          await ImagePicker.pickImage(source: source, imageQuality: quality);
    else
      photo = await ImagePicker.pickImage(source: source);

    setState(() {
      _photo = photo;
    });
  }
}
