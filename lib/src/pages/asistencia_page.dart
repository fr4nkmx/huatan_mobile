import 'package:attendance/src/bloc/bloc_provider.dart';
import 'package:attendance/src/models/asistencia_mes.dart';
import 'package:attendance/src/models/asistencia_semana.dart';
import 'package:attendance/src/widgets/app_drawer.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';

class AsistenciaPage extends StatefulWidget {
  const AsistenciaPage({Key key}) : super(key: key);

  @override
  _AsistenciaPageState createState() => _AsistenciaPageState();
}

class _AsistenciaPageState extends State<AsistenciaPage> {
  final numSemana = Jiffy().week;
  double _width = 0.0;
  double _height = 0.0;

  double _scale = 0.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _width = MediaQuery.of(context).size.width;
    _height = MediaQuery.of(context).size.height;
    _scale = _height / 32.0;

    final asistenciaBloc = BlocProvider.asistenciaBloc(context);
    asistenciaBloc.resumenSemana();
    asistenciaBloc.resumenMes();

    return Scaffold(
      appBar: AppBar(
        title: Text('ASISTENCIA'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Card(
              elevation: 6.0,
              child: Container(
                padding: new EdgeInsets.all(_height / 46.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text(
                      'SEMANA',
                      style: TextStyle(
                        fontSize: _scale,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                    Text(
                      '$numSemana',
                      style: TextStyle(
                        fontSize: _scale,
                        fontWeight: FontWeight.w900,
                      ),
                    )
                  ],
                ),
              ),
            ),
            StreamBuilder<List<AsistenciaSemana>>(
                stream: asistenciaBloc.resumenSemanaStream,
                initialData: defaultSemanaData(),
                builder: (context, snapshot) {
                  // switch (snapshot.connectionState) {
                  //   case ConnectionState.waiting:
                  //     print('WAITING');
                  //     break;
                  //   case ConnectionState.active:
                  //     print('### ACTIVE ###');
                  //     break;
                  //   case ConnectionState.none:
                  //     print('.### NONE ###');
                  //     break;
                  //   case ConnectionState.done:
                  //     print('### DONE ###');
                  //     break;
                  //   default:
                  //     print('### UNKNOW ####');
                  // }

                  return Card(
                    elevation: 6.0,
                    child: Column(
                      children: _diasWidget(snapshot.data),
                    ),
                  );
                }),
            StreamBuilder<AsistenciaMes>(
              stream: asistenciaBloc.resumenMesStream,
              initialData:
                  AsistenciaMes(asistencias: 0, faltas: 0, retardos: 0),
              builder: (context, snapshot) {
                return Card(
                  elevation: 6.0,
                  child: Container(
                    padding: new EdgeInsets.symmetric(
                        horizontal: _width / 24.0, vertical: _height / 42.0),
                    child: Column(
                      children: <Widget>[
                        Center(
                          child: Text('RESUMEN DEL MES',
                              style: TextStyle(
                                fontSize: _scale,
                                fontWeight: FontWeight.w900,
                              )),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Expanded(
                              child: _itemResume(
                                snapshot.hasData ? Colors.green : Colors.grey,
                                'Asistencias',
                                snapshot.hasData
                                    ? '${snapshot.data.asistencias}'
                                    : '0',
                              ),
                            ),
                            SizedBox(
                              width: 5.0,
                            ),
                            Expanded(
                              child: _itemResume(
                                snapshot.hasData ? Colors.orange : Colors.grey,
                                'Retardos',
                                snapshot.hasData
                                    ? '${snapshot.data.retardos}'
                                    : '0',
                              ),
                            ),
                            SizedBox(
                              width: 5.0,
                            ),
                            Expanded(
                              child: _itemResume(
                                snapshot.hasData ? Colors.red : Colors.grey,
                                'Faltas',
                                snapshot.hasData
                                    ? '${snapshot.data.faltas}'
                                    : '0',
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
            )
          ],
        ),
      ),
      drawer: AppDrawer(),
    );
  }

  List<Widget> _diasWidget(List<AsistenciaSemana> data) {
    print(data);

    List<Widget> result = new List<Widget>();

    data.forEach((day) {
      result.add(_doDay(
        day.index,
        _colorStatus(day.asistencia, day.retardo, day.falta),
        day.diaSemana,
        day.horaEntrada,
        day.horaSalida,
      ));
    });

    return result;
  }

  _colorStatus(bool attendence, bool retardo, bool falta) {
    if (attendence) return Colors.green;
    if (retardo) return Colors.orange;
    if (falta) return Colors.red;
    return Colors.grey;
  }

  Widget _doDay(int index, Color color, String day, String ini, String out) {
    final time = (ini + out).isNotEmpty ? '$ini - $out' : 'PENDIENTE';

    return ListTile(
      dense: true,
      title: Text(day,
          style: TextStyle(
            fontSize: _scale,
            fontWeight: FontWeight.w900,
          )),
      subtitle: Row(
        children: <Widget>[
          Text('$time'),
        ],
      ),
      trailing: CircleAvatar(
        backgroundColor: color,
        radius: _scale,
      ),
    );
  }

  Widget _itemResume(Color color, String text, String count) {
    const TextStyle countStyle = TextStyle(
        fontWeight: FontWeight.bold, fontSize: 20.0, color: Colors.white);

    const TextStyle textStyle =
        TextStyle(fontWeight: FontWeight.bold, color: Colors.white);

    return Container(
      padding: const EdgeInsets.all(8.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: color,
      ),
      child: Column(
        children: <Widget>[
          Text(text, style: textStyle),
          const SizedBox(height: 5.0),
          Text(
            count,
            style: countStyle,
          )
        ],
      ),
    );
  }

  List<AsistenciaSemana> defaultSemanaData() {
    var result = new List<AsistenciaSemana>();

    var dias = [
      'DOMINGO',
      'LUNES',
      'MARTES',
      'MIERCOLES',
      'JUEVES',
      'VIERNES',
      'SABADO',      
    ];

    dias.asMap().forEach((index, value) => {
          result.add(AsistenciaSemana(
              index: index,
              diaSemana: value,
              horaEntrada: '',
              horaSalida: '',
              asistencia: false,
              retardo: false,
              falta: false))
        });
    return result;
  }
}
