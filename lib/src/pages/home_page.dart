import 'dart:ui';
import 'package:attendance/src/bloc/bloc_provider.dart';
import 'package:attendance/src/models/asistencia_state_model.dart';
import 'package:attendance/src/models/empleado_model.dart';

import 'package:flutter/material.dart';

import 'package:attendance/src/widgets/app_drawer.dart';

class HomePage extends StatefulWidget {
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String entrada = '';
  String salida = '';

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Container(
            child: Image.asset(
          'assets/huatan_white.png',
          fit: BoxFit.contain,
          height: 32,
        )),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              child: Center(
                child: Column(
                  children: <Widget>[
                    _empleadoWidget(context),
                    _tramoWidget(context),
                    _statusWidget(context),
                    _accionesWidget(context),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      drawer: AppDrawer(),
    );
  }

  Widget _empleadoWidget(BuildContext context) {
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;
    final blocEmpleado = BlocProvider.empleadoBloc(context);

    // QUE PASA SI NO EXISTE INFORMACION DEL EMPLEADO O TRAMO
    return StreamBuilder<EmpleadoModel>(
        stream: blocEmpleado.empleadoStream,
        builder: (context, snapshot) {
          return Container(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: _height / 25,
                ),
                CircleAvatar(
                  backgroundColor: Colors.grey,
                  radius: _width < _height ? _width / 4 : _height / 4,
                  backgroundImage: AssetImage('assets/profile-tmp.png'),
                ),
                SizedBox(
                  height: _height / 50,
                ),
                Text(
                  snapshot.hasData
                      ? '${snapshot.data.nombre} ${snapshot.data.paterno} ${snapshot.data.materno}'
                      : 'Cargando...',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: _width / 20,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _tramoWidget(BuildContext context) {
    final _width = MediaQuery.of(context).size.width;
    final blocEmpleado = BlocProvider.empleadoBloc(context);

    return StreamBuilder<EmpleadoModel>(
        stream: blocEmpleado.empleadoStream,
        builder: (context, snapshot) {
          print(snapshot.data);
          return Container(
            child: Column(
              children: <Widget>[
                Text(
                  'TRAMO',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: _width / 20,
                  ),
                ),
                Text(
                  snapshot.hasData ? snapshot.data.obra  : 'Cargando...',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: _width / 15,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _accionesWidget(BuildContext context) {
    final asistenciaBloc = BlocProvider.asistenciaBloc(context);
    asistenciaBloc.init();

    return StreamBuilder<AsistenciaStateModel>(
        stream: asistenciaBloc.asistenciaStateStream,
        builder: (BuildContext context,
            AsyncSnapshot<AsistenciaStateModel> snapshot) {
          if (snapshot.hasData) {
            print('### ESTADO DE LA CONEXION ###');
            print(snapshot.connectionState);

            print('### DATA ###');
            print(snapshot.data);

            return Column(
              children: <Widget>[
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                        child: Column(
                          children: <Widget>[
                            IconButton(
                                iconSize: 76.0,
                                icon: Icon(Icons.timer),
                                color: Colors.greenAccent,
                                onPressed: !snapshot.data.entradaEnable
                                    ? null
                                    : () {
                                        print(
                                            'COMENZAMOS EL TRACKING Y EL LOGGING A FIREBASE');
                                        asistenciaBloc.checkin();
                                      }),
                            Text(snapshot.data.horaEntrada != ''
                                ? snapshot.data.horaEntrada
                                : 'REGISTRAR ENTRADA')
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          children: <Widget>[
                            IconButton(
                              iconSize: 76.0,
                              icon: Icon(Icons.timer_off),
                              color: Colors.redAccent,
                              onPressed: !snapshot.data.salidaEnable
                                  ? null
                                  : () {
                                      print(
                                          'CERRAMOS EL TRACKING Y EL LOGGING A FIREBASE');
                                      asistenciaBloc.checkout();
                                    },
                            ),
                            Text(snapshot.data.horaSalida != ''
                                ? snapshot.data.horaSalida
                                : 'REGISTRAR SALIDA'),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            );
          } else
            return CircularProgressIndicator();
        });
  }

  Widget _statusWidget(BuildContext context) {
    final geofenceBloc = BlocProvider.geofenceBloc(context);
    final asistenciaBloc = BlocProvider.asistenciaBloc(context);

    return StreamBuilder(
      stream: geofenceBloc.geofenceEventStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if ('GeofenceEvent.exit' == snapshot.data)
            asistenciaBloc.enableEntrada(false);
          else
            asistenciaBloc.enableEntrada(true);

          return Center(
            child: _geoFenceStatus(snapshot.data),
          );
        } else {
          return Center(
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.gps_not_fixed,
                  size: 45.0,
                  color: Colors.orange,
                ),
                Text('Obteniendo datos de GPS'),
              ],
            ),
          );
        }
      },
    );
  }

  Widget _geoFenceStatus(String event) {
    switch (event) {
      case 'GeofenceEvent.enter':
        return _iconFenceStatus(
          Icons.check_circle,
          Colors.green,
          'Puedes confirmar tu asistencia/salida',
        );
        break;

      case 'GeofenceEvent.dwell':
        return _iconFenceStatus(
          Icons.check_circle_outline,
          Colors.grey,
          'Puedes confirmar tu asistencia/salida',
        );
        break;

      case 'GeofenceEvent.exit':
        return _iconFenceStatus(
          Icons.block,
          Colors.red,
          'Fuera de la zona de trabajo',
        );
        break;
      default:
        return _iconFenceStatus(
          Icons.gps_not_fixed,
          Colors.orange,
          'Obteniendo datos de GPS',
        );
    }
  }

  Widget _iconFenceStatus(IconData icon, Color color, String text) {
    return Column(
      children: <Widget>[
        Icon(
          icon,
          size: 45.0,
          color: color,
        ),
        Text(text)
      ],
    );
  }
}
