import 'package:attendance/src/utils/preferences.dart';
import 'package:flutter/material.dart';

class NoRegistradoPage extends StatelessWidget {
  final _prefs = Preferences();

  @override
  Widget build(BuildContext context) {
    _prefs.resEmpleado = null;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Container(
            child: Image.asset(
          'assets/huatan_white.png',
          fit: BoxFit.contain,
          height: 32,
        )),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Text(
              'No te encuentras registrado,',
              style: TextStyle(fontSize: 24.0),
            ),
          ),
          Center(
            child: Text(
              'Contacta con tu superior',
              style: TextStyle(fontSize: 24.0),
            ),
          ),
        ],
      ),
    );
  }
}
