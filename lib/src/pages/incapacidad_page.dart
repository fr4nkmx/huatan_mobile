import 'package:attendance/src/bloc/bloc_provider.dart';
import 'package:attendance/src/models/incapacidad_model.dart';
import 'package:attendance/src/widgets/app_drawer.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';

class IncapacidadPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('INCAPACIDADES'),
        centerTitle: true,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Card(
            elevation: 6.0,
            child: Container(
              padding: EdgeInsets.all(18.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text(
                        'Seguimiento de incapacidades',
                        style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Card(elevation: 6.0, child: _incapacidadesWidget(context)),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.green,
        onPressed: () {
          print('Agregar una nueva incapacidad');
          Navigator.of(context).pushNamed('add_incapacidad');
        },
      ),
      drawer: AppDrawer(),
    );
  }

  StreamBuilder<List<IncapacidadModel>> _incapacidadesWidget(
      BuildContext context) {
    final bloc = BlocProvider.incapacidadBloc(context);
    bloc.getAll();

    return StreamBuilder(
      stream: bloc.incapacidadesStream,
      builder: (BuildContext context,
          AsyncSnapshot<List<IncapacidadModel>> snapshot) {
        final items = snapshot.data ?? List();

        return ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: items.length,
            itemBuilder: (BuildContext context, int index) {
              final item = items[index];
              return ListTile(
                title: Text(item.motivo),
                subtitle: Text(
                    Jiffy(item.fechaSolicitud).format("dd-MM-yyyy").toString()),
                trailing: Text(_resolveStatus(item.estadoProceso)),
                onTap: () {
                  print(
                      'enviar a una pantalla detalle con la imagen de la receta');
                },
              );
            });
      },
    );
  }

  String _resolveStatus(int status) {
    switch (status) {
      case 0:
        return 'PENDIENTE';
      case 1:
        return 'REVISION';
      case 2:
        return 'APROBADO';
      case 3:
        return 'DECLINADO';
      default:
        return '';
    }
  }
}
