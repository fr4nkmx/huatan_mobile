import 'package:attendance/src/bloc/bloc_provider.dart';
import 'package:attendance/src/bloc/vacacion_bloc.dart';
import 'package:attendance/src/models/vacacion_model.dart';
import 'package:attendance/src/provider/vacacion_provider.dart';

import 'package:attendance/src/widgets/app_drawer.dart';
import 'package:flutter/material.dart';

import 'package:flutter_date_pickers/flutter_date_pickers.dart';
import 'package:progress_dialog/progress_dialog.dart';

class VacacionesPage extends StatefulWidget {
  const VacacionesPage({Key key}) : super(key: key);

  @override
  _VacacionesPageState createState() => _VacacionesPageState();
}

class _VacacionesPageState extends State<VacacionesPage> {
  DateTime _firstDate;
  DateTime _lastDate;
  DatePeriod _selectedPeriod;

  ProgressDialog pr;
  Color selectedPeriodStartColor = Colors.greenAccent;
  Color selectedPeriodLastColor = Colors.greenAccent;
  Color selectedPeriodMiddleColor = Colors.blue;
  bool _isDisabled = false;
  double _height = 0.0;
  double _scale = 0.0;
  VacacionBloc bloc;

  @override
  void initState() {
    super.initState();
    _firstDate = DateTime.now();
    _lastDate = DateTime.now().add(Duration(days: 45));

    DateTime selectedPeriodStart = DateTime.now();
    DateTime selectedPeriodEnd = DateTime.now().add(Duration(days: 8));
    _selectedPeriod = DatePeriod(selectedPeriodStart, selectedPeriodEnd);
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal);
    pr.style(message: 'Enviando información');

    bloc = BlocProvider.vacacionBloc(context);
    bloc.get();

    _height = MediaQuery.of(context).size.height;
    _scale = _height / 36.0;

    DatePickerRangeStyles styles = DatePickerRangeStyles(
      selectedPeriodLastDecoration: BoxDecoration(
        color: selectedPeriodLastColor,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(10.0),
          bottomRight: Radius.circular(10.0),
        ),
      ),
      selectedPeriodStartDecoration: BoxDecoration(
        color: selectedPeriodStartColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10.0),
          bottomLeft: Radius.circular(10.0),
        ),
      ),
      selectedPeriodMiddleDecoration: BoxDecoration(
        color: selectedPeriodMiddleColor,
        shape: BoxShape.rectangle,
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('VACACIONES'),
        centerTitle: true,
      ),
      body: Container(
        child: StreamBuilder<VacacionModel>(
          stream: bloc.vacacionStream,
          builder:
              (BuildContext context, AsyncSnapshot<VacacionModel> snapshot) {
            if (snapshot.hasError) {
              if (snapshot.error as int == 0)
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(
                      child: Text(
                        'Intentalo mas tarde',
                        style: TextStyle(fontSize: _scale, color: Colors.red),
                      ),
                    )
                  ],
                );
              if (snapshot.error as int == 101)
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(
                      child: Text(
                        'Verifica tu conexión a internet',
                        style: TextStyle(fontSize: _scale, color: Colors.red),
                      ),
                    )
                  ],
                );
              if (snapshot.error as int >= 500)
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(
                      child: Text(
                        'Error al consultar al servidor',
                        style: TextStyle(fontSize: _scale, color: Colors.red),
                      ),
                    )
                  ],
                );
              if (snapshot.error as int == 404)
                return Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Center(
                            child: Text(
                              'SOLICITUD DE VACACIONES',
                              style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.w900,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          RangePicker(
                            selectedPeriod: _selectedPeriod,
                            onChanged: _onSelectedDateChanged,
                            firstDate: _firstDate,
                            lastDate: _lastDate,
                            datePickerStyles: styles,
                          ),
                          FlatButton(
                            onPressed: _isDisabled
                                ? null
                                : () async {
                                    pr.show();
                                    await _sendInfo().then((res) {
                                      setState(() {
                                        _isDisabled = true;
                                      });
                                      pr.hide();
                                    });
                                  },
                            color: Colors.blue,
                            child: Text('Enviar'),
                          ),
                        ],
                      ),
                    ]);
            }

            if (!snapshot.hasData)
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Center(
                    child: CircularProgressIndicator(),
                  )
                ],
              );

            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(padding: EdgeInsets.only(top: _scale)),
                _infoVacacion(
                  snapshot.data.fechaSolicitud,
                  snapshot.data.ini,
                  snapshot.data.fin,
                ),
                Padding(padding: EdgeInsets.only(top: _scale)),
                _resolveStatus(snapshot.data.estadoProceso),
              ],
            );
          },
        ),
      ),
      drawer: AppDrawer(),
    );
  }

  Widget _resolveStatus(int status) {
    String result = '';
    Color colorStatus = Colors.orange;
    switch (status) {
      case 0:
        result = '$result  PENDIENTE';
        colorStatus = Colors.orange;
        break;
      case 1:
        result = '$result  REVISION';
        colorStatus = Colors.orangeAccent;
        break;
      case 2:
        result = '$result  APROBADO';
        colorStatus = Colors.green;
        break;
      case 3:
        result = '$result  DECLINADO';
        colorStatus = Colors.red;
        break;
      default:
        result = '$result   PENDIENTE';
    }

    return Text(result,
        style: TextStyle(
          fontSize: 18.0,
          fontWeight: FontWeight.w900,
          color: colorStatus,
        ));
  }

  Widget _infoVacacion(String fecha, String ini, String fin) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          'ESTADO SOLICITUD REALIZADA',
          style: TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.w900,
          ),
        ),
        Text(
          fecha,
          style: TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.w900,
          ),
        ),
        Padding(padding: EdgeInsets.only(top: _scale)),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            _fecha('DE:', ini),
            _fecha('A:', fin),
          ],
        )
      ],
    );
  }

  Widget _fecha(String prefix, String fecha) {
    final style = TextStyle(
      fontSize: 18.0,
      fontWeight: FontWeight.w900,
    );

    return Row(
      children: <Widget>[
        Text(
          prefix,
          style: style,
        ),
        Text(
          fecha,
          style: style,
        )
      ],
    );
  }

  Future _sendInfo() async {
    final provider = VacacionProvider();
    await provider.add(_selectedPeriod.start, _selectedPeriod.end);
  }

  _onSelectedDateChanged(DatePeriod newPeriod) {
    print(newPeriod.start);
    setState(() {
      _selectedPeriod = newPeriod;
    });
  }
}
