import 'package:attendance/src/bloc/otp_bloc.dart';
import 'package:attendance/src/pages/otp_page.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = OtpBloc();

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "NUMERO TELEFONICO",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding:
                    const EdgeInsets.only(left: 16.0, top: 20.0, right: 16.0),
                child: Text(
                  "Ingresa tu número de teléfono",
                  style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: Image(
                  image: AssetImage('assets/images/otp-icon.png'),
                  height: 120.0,
                  width: 120.0,
                ),
              ),
              Row(
                children: <Widget>[
                  Flexible(
                    child: Container(),
                    flex: 1,
                  ),
                  Flexible(
                    child: TextFormField(
                      textAlign: TextAlign.center,
                      autofocus: false,
                      enabled: false,
                      initialValue: "+52",
                      style: TextStyle(fontSize: 20.0, color: Colors.black),
                    ),
                    flex: 3,
                  ),
                  Flexible(
                    child: Container(),
                    flex: 1,
                  ),
                  Flexible(
                    child: StreamBuilder(
                        stream: bloc.telefonoStream,
                        initialData: null,
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          return Container(
                            child: TextFormField(
                              initialValue: '',
                              textAlign: TextAlign.start,
                              autofocus: false,
                              enabled: true,
                              keyboardType: TextInputType.number,
                              textInputAction: TextInputAction.done,
                              decoration: InputDecoration(
                                errorText: snapshot?.error,
                              ),
                              style: TextStyle(
                                fontSize: 20.0,
                                color: Colors.black,
                              ),
                              onChanged: bloc.changeTelefono,
                            ),
                          );
                        }),
                    flex: 7,
                  ),
                  Flexible(
                    child: Container(),
                    flex: 1,
                  ),
                  Flexible(
                    child: StreamBuilder(
                      stream: bloc.telefonoStream,
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (snapshot.hasData) {
                          return Icon(
                            Icons.done,
                            color: Colors.green,
                          );
                        } else {
                          return Container();
                        }
                      },
                    ),
                    flex: 2,
                  )
                ],
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 40.0, horizontal: 40.0),
                child: StreamBuilder<String>(
                    stream: bloc.telefonoStream,
                    builder: (context, snapshot) {
                      return Container(
                        child: RaisedButton(
                          onPressed: snapshot?.data == null
                              ? null
                              : () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => OtpPage(
                                              telefono: snapshot.data)));
                                },
                          child: Text("SOLICITAR CODIGO SMS"),
                          textColor: Colors.white,
                          color: Colors.blue,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        ),
                      );
                    }),
              )
            ],
          )
        ],
      ),
    );
  }
}
