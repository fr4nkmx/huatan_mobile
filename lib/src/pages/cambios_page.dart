import 'package:attendance/src/bloc/bloc_provider.dart';
import 'package:attendance/src/bloc/cambio_tramo_bloc.dart';
import 'package:attendance/src/models/cambio_tramo_model.dart';
import 'package:attendance/src/widgets/app_drawer.dart';
import 'package:flutter/material.dart';

class CambiosPage extends StatefulWidget {
  const CambiosPage({Key key}) : super(key: key);

  @override
  _CambiosPageState createState() => _CambiosPageState();
}

class _CambiosPageState extends State<CambiosPage> {
  double _height = 0.0;
  double _scale = 0.0;

  CambioTramoBloc bloc;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _scale = _height / 36.0;

    bloc = BlocProvider.cambioTramoBloc(context);
    bloc.get();
    return Scaffold(
      appBar: AppBar(
        title: Text('CAMBIO DE TRAMO'),
        centerTitle: true,
      ),
      body: Container(
        child: StreamBuilder<CambioTramoModel>(
            stream: bloc.cambioTramoStream,
            builder: (BuildContext context,
                AsyncSnapshot<CambioTramoModel> snapshot) {
              if (snapshot.hasError) {
                if (snapshot.error as int == 0)
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Text(
                          'Intentalo mas tarde',
                          style: TextStyle(fontSize: _scale, color: Colors.red),
                        ),
                      )
                    ],
                  );
                if (snapshot.error as int == 101)
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Text(
                          'Verifica tu conexión a internet',
                          style: TextStyle(fontSize: _scale, color: Colors.red),
                        ),
                      )
                    ],
                  );
                if (snapshot.error as int >= 500)
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Text(
                          'Error al consultar al servidor',
                          style: TextStyle(fontSize: _scale, color: Colors.red),
                        ),
                      )
                    ],
                  );
                if (snapshot.error as int == 404)
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Text(
                          'Sin cambio de tramo programado',
                          style: TextStyle(fontSize: _scale),
                        ),
                      )
                    ],
                  );
              }

              if (!snapshot.hasData)
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(
                      child: CircularProgressIndicator(),
                    )
                  ],
                );

              return Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  _title(),
                  _staticMap(
                      snapshot.data.destinoLat, snapshot.data.destinoLon),
                  Padding(padding: EdgeInsets.only(top: _scale)),
                  _infoTramo(snapshot.data.destino, snapshot.data.ini,
                      snapshot.data.fin),
                  Padding(padding: EdgeInsets.only(top: _scale)),
                  _buttonEnvio(snapshot.data.estadoNotificacion)
                ],
              );
            }),
      ),
      drawer: AppDrawer(),
    );
  }

  Widget _title() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 0.0),
      child: Center(
          child: Text('SE ASIGNO A UN NUEVO TRAMO',
              style: TextStyle(
                  fontSize: _scale / 1.25, fontWeight: FontWeight.bold))),
    );
  }

  Widget _buttonEnvio(int status) {
    print('### STATUS ###');
    print(status);

    String msg = status == 3
        ? 'Confirmación enviada'
        : 'Enviar confirmación de enterado';

    return FlatButton(
      onPressed: status == 3
          ? null
          : () {
              bloc.update(3);
            },
      color: Colors.blue,
      disabledColor: Colors.grey,
      child: Text(
        msg,
        style: TextStyle(
          color: Colors.white,
          fontSize: 18.0,
          fontWeight: FontWeight.w900,
        ),
      ),
    );
  }

  Widget _staticMap(double lat, double lon) {
    String apiKey = 'AIzaSyBUdyo6m3p6ognhG61UJG8cHTUWXcKXFBM';
    String url =
        'https://maps.googleapis.com/maps/api/staticmap?center=$lat,$lon&zoom=18&scale=1&size=600x400&maptype=roadmap&key=$apiKey&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff8080%7Clabel:%7C$lat,$lon';

    return Container(
      child: Image.network(url),
    );
  }

  Widget _infoTramo(String destino, String ini, String fin) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: Icon(
                Icons.place,
                color: Colors.redAccent,
                size: 35.0,
              ),
              flex: 2,
            ),
            Expanded(
              child: Text(
                'ASIGNACION',
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.w900,
                ),
              ),
              flex: 4,
            ),
            Expanded(
              child: Text(destino.toUpperCase(),
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.w900,
                  )),
              flex: 6,
            )
          ],
        ),
        Padding(padding: EdgeInsets.only(top: _scale)),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            _fecha('DE:', ini),
            _fecha('A:', fin),
          ],
        )
      ],
    );
  }

  Widget _fecha(String prefix, String fecha) {
    final style = TextStyle(
      fontSize: 18.0,
      fontWeight: FontWeight.w900,
    );

    return Row(
      children: <Widget>[
        Text(
          prefix,
          style: style,
        ),
        Text(
          fecha,
          style: style,
        )
      ],
    );
  }
}
