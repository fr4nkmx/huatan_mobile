// To parse this JSON data, do
//
//     final vacacionModel = vacacionModelFromJson(jsonString);

import 'dart:convert';

VacacionModel vacacionModelFromJson(String str) =>
    VacacionModel.fromJson(json.decode(str));

String vacacionModelToJson(VacacionModel data) => json.encode(data.toJson());

class VacacionModel {
  int id;
  String fechaSolicitud;
  String ini;
  String fin;
  int estadoProceso;

  VacacionModel({
    this.id,
    this.fechaSolicitud,
    this.ini,
    this.fin,
    this.estadoProceso,
  });

  factory VacacionModel.fromJson(Map<String, dynamic> json) => VacacionModel(
        id: json["id"],
        fechaSolicitud: json["fechaSolicitud"],
        ini: json["ini"],
        fin: json["fin"],
        estadoProceso: json["estadoProceso"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "fechaSolicitud": fechaSolicitud,
        "ini": ini,
        "fin": fin,
        "estadoProceso": estadoProceso,
      };
}
