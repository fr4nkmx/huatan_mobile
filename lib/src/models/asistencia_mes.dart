// To parse this JSON data, do
//
//     final asistenciaMes = asistenciaMesFromJson(jsonString);

import 'dart:convert';

AsistenciaMes asistenciaMesFromJson(String str) =>
    AsistenciaMes.fromMap(json.decode(str));

String asistenciaMesToJson(AsistenciaMes data) => json.encode(data.toMap());

class AsistenciaMes {
  int asistencias;
  int retardos;
  int faltas;

  AsistenciaMes({
    this.asistencias,
    this.retardos,
    this.faltas,
  });

  factory AsistenciaMes.fromMap(Map<String, dynamic> json) => AsistenciaMes(
        asistencias: json["asistencias"],
        retardos: json["retardos"],
        faltas: json["faltas"],
      );

  Map<String, dynamic> toMap() => {
        "asistencias": asistencias,
        "retardos": retardos,
        "faltas": faltas,
      };
}
