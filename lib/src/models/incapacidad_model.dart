// To parse this JSON data, do
//
//     final incapacidadModel = incapacidadModelFromJson(jsonString);

import 'dart:convert';

IncapacidadModel incapacidadModelFromJson(String str) =>
    IncapacidadModel.fromJson(json.decode(str));

String incapacidadModelToJson(IncapacidadModel data) =>
    json.encode(data.toJson());

class IncapacidadModel {
  int incapacidadId;
  int estadoProceso;
  String fechaSolicitud;
  String fechaRespuesta;
  String urlImagen;
  String motivo;

  IncapacidadModel({
    this.incapacidadId,
    this.estadoProceso,
    this.fechaSolicitud,
    this.fechaRespuesta,
    this.urlImagen,
    this.motivo,
  });

  factory IncapacidadModel.fromJson(Map<String, dynamic> json) =>
      IncapacidadModel(
        incapacidadId: json["incapacidadId"],
        estadoProceso: json["estadoProceso"],
        fechaSolicitud: json["fechaSolicitud"],
        fechaRespuesta: json["fechaRespuesta"],
        urlImagen: json["urlImagen"],
        motivo: json["motivo"],
      );

  Map<String, dynamic> toJson() => {
        "incapacidadId": incapacidadId,
        "estadoProceso": estadoProceso,
        "fechaSolicitud": fechaSolicitud,
        "fechaRespuesta": fechaRespuesta,
        "urlImagen": urlImagen,
        "motivo": motivo,
      };
}
