// To parse this JSON data, do
//
//     final asistenciaModel = asistenciaModelFromJson(jsonString);

import 'dart:convert';

AsistenciaModel asistenciaModelFromJson(String str) =>
    AsistenciaModel.fromMap(json.decode(str));

String asistenciaModelToJson(AsistenciaModel data) => json.encode(data.toMap());

class AsistenciaModel {
  int asistenciaId;
  String fecha;
  String horaEntrada;
  String horaSalida;
  bool asistencia;
  bool retardo;
  bool falta;
  bool noLaboral;

  AsistenciaModel({
    this.asistenciaId,
    this.fecha,
    this.horaEntrada,
    this.horaSalida,
    this.asistencia = false, // 0
    this.retardo = false, // 1
    this.falta = true, // 2
    this.noLaboral = false, // 3
  });

  factory AsistenciaModel.fromMap(Map<String, dynamic> json) => AsistenciaModel(
        asistenciaId: json["asistenciaId"],
        fecha: json["fecha"],
        horaEntrada: json["horaEntrada"],
        horaSalida: json["horaSalida"],
        asistencia: json["asistencia"],
        retardo: json["retardo"],
        falta: json["falta"],
        noLaboral: json["noLaboral"],
      );

  Map<String, dynamic> toMap() => {
        "asistenciaId": asistenciaId,
        "fecha": fecha,
        "horaEntrada": horaEntrada,
        "horaSalida": horaSalida,
        "asistencia": asistencia,
        "retardo": retardo,
        "falta": falta,
        "noLaboral": noLaboral,
      };
}
