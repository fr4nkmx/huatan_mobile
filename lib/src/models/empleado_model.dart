// To parse this JSON data, do
//
//     final empleadoModel = empleadoModelFromJson(jsonString);

import 'dart:convert';

EmpleadoModel empleadoModelFromJson(String str) =>
    EmpleadoModel.fromJson(json.decode(str));

String empleadoModelToJson(EmpleadoModel data) => json.encode(data.toJson());

class EmpleadoModel {
  int id;
  String numNomina;
  String nombre;
  String paterno;
  String materno;
  String puesto;
  String obra;
  TramoModel tramoModel;
  String telefono;
  double monto;
  String operador;
  bool status;
  String fcmToken;
  dynamic deviceId;
  dynamic firebaseId;
  List<GeocercaModel> geocercaModel;

  EmpleadoModel({
    this.id,
    this.numNomina,
    this.nombre,
    this.paterno,
    this.materno,
    this.puesto,
    this.obra,
    this.tramoModel,
    this.telefono,
    this.monto,
    this.operador,
    this.status,
    this.fcmToken,
    this.deviceId,
    this.firebaseId,
    this.geocercaModel,
  });

  factory EmpleadoModel.fromJson(Map<String, dynamic> json) => EmpleadoModel(
        id: json["id"] == null ? null : json["id"],
        numNomina: json["numNomina"] == null ? null : json["numNomina"],
        nombre: json["nombre"] == null ? null : json["nombre"],
        paterno: json["paterno"] == null ? null : json["paterno"],
        materno: json["materno"] == null ? null : json["materno"],
        puesto: json["puesto"] == null ? null : json["puesto"],
        obra: json["obra"] == null ? null : json["obra"],
        tramoModel: json["tramoModel"] == null
            ? null
            : TramoModel.fromJson(json["tramoModel"]),
        telefono: json["telefono"] == null ? null : json["telefono"],
        monto: json["monto"] == null ? null : json["monto"],
        operador: json["operador"] == null ? null : json["operador"],
        status: json["status"] == null ? null : json["status"],
        fcmToken: json["fcmToken"] == null ? null : json["fcmToken"],
        deviceId: json["deviceId"],
        firebaseId: json["firebaseId"],
        geocercaModel: json["geocercaModel"] == null
            ? null
            : List<GeocercaModel>.from(
                json["geocercaModel"].map((x) => GeocercaModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "numNomina": numNomina == null ? null : numNomina,
        "nombre": nombre == null ? null : nombre,
        "paterno": paterno == null ? null : paterno,
        "materno": materno == null ? null : materno,
        "puesto": puesto == null ? null : puesto,
        "obra": obra == null ? null : obra,
        "tramoModel": tramoModel == null ? null : tramoModel.toJson(),
        "telefono": telefono == null ? null : telefono,
        "monto": monto == null ? null : monto,
        "operador": operador == null ? null : operador,
        "status": status == null ? null : status,
        "fcmToken": fcmToken == null ? null : fcmToken,
        "deviceId": deviceId,
        "firebaseId": firebaseId,
        "geocercaModel": geocercaModel == null
            ? null
            : List<dynamic>.from(geocercaModel.map((x) => x.toJson())),
      };
}

class GeocercaModel {
  String zona;
  double lat;
  double lon;
  double rad;

  GeocercaModel({
    this.zona,
    this.lat,
    this.lon,
    this.rad,
  });

  factory GeocercaModel.fromJson(Map<String, dynamic> json) => GeocercaModel(
        zona: json["zona"] == null ? null : json["zona"],
        lat: json["lat"] == null ? null : json["lat"].toDouble(),
        lon: json["lon"] == null ? null : json["lon"].toDouble(),
        rad: json["rad"] == null ? null : json["rad"],
      );

  Map<String, dynamic> toJson() => {
        "zona": zona == null ? null : zona,
        "lat": lat == null ? null : lat,
        "lon": lon == null ? null : lon,
        "rad": rad == null ? null : rad,
      };
}

class TramoModel {
  int id;
  double lat;
  double lon;
  String zona;
  String nombre;
  String iniVialidad;
  String finVialidad;

  TramoModel({
    this.id,
    this.lat,
    this.lon,
    this.zona,
    this.nombre,
    this.iniVialidad,
    this.finVialidad,
  });

  factory TramoModel.fromJson(Map<String, dynamic> json) => TramoModel(
        id: json["id"] == null ? null : json["id"],
        lat: json["lat"] == null ? null : json["lat"].toDouble(),
        lon: json["lon"] == null ? null : json["lon"].toDouble(),
        zona: json["zona"] == null ? null : json["zona"],
        nombre: json["nombre"] == null ? null : json["nombre"],
        iniVialidad: json["iniVialidad"] == null ? null : json["iniVialidad"],
        finVialidad: json["finVialidad"] == null ? null : json["finVialidad"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "lat": lat == null ? null : lat,
        "lon": lon == null ? null : lon,
        "zona": zona == null ? null : zona,
        "nombre": nombre == null ? null : nombre,
        "iniVialidad": iniVialidad == null ? null : iniVialidad,
        "finVialidad": finVialidad == null ? null : finVialidad,
      };
}
