// To parse this JSON data, do
//
//     final asistenciaStateModel = asistenciaStateModelFromJson(jsonString);

import 'dart:convert';

AsistenciaStateModel asistenciaStateModelFromJson(String str) =>
    AsistenciaStateModel.fromMap(json.decode(str));

String asistenciaStateModelToJson(AsistenciaStateModel data) =>
    json.encode(data.toMap());

class AsistenciaStateModel {
  bool entradaEnable;
  bool salidaEnable;  
  bool terminaJornada;
  String horaEntrada;
  String horaSalida;
  int dia;
  int dayOfYear;
  int week;

  AsistenciaStateModel(
      {this.entradaEnable = false,
      this.salidaEnable = false,
      this.terminaJornada = false,
      this.horaEntrada = "",
      this.horaSalida = "",
      this.dia,
      this.dayOfYear,
      this.week});

  factory AsistenciaStateModel.fromMap(Map<String, dynamic> json) =>
      AsistenciaStateModel(
        entradaEnable: json["entradaEnable"],
        salidaEnable: json["salidaEnable"],
        terminaJornada: json["terminaJornada"],
        horaEntrada: json["horaEntrada"],
        horaSalida: json["horaSalida"],
        dia: json["dia"],
        dayOfYear: json["dayOfYear"],
        week: json["week"],
      );

  Map<String, dynamic> toMap() => {
        "entradaEnable": entradaEnable,
        "salidaEnable": salidaEnable,
        "terminaJornada": terminaJornada,
        "horaEntrada": horaEntrada,
        "horaSalida": horaSalida,
        "dia": dia,
        "dayOfYear": dayOfYear,
        "week": week
      };
}
