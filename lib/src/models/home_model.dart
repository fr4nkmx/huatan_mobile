import 'package:geofencing/geofencing.dart';

class HomeModel {
  double lat;
  double lon;
  String zona;
  String entrada;
  String salida;
  DateTime fecha;
  String telefono;
  String token;
  GeofenceEvent geofenceEvent;

  HomeModel({
    this.lat,
    this.lon,
    this.zona,
    this.entrada,
    this.salida,
    this.fecha,
    this.telefono,
    this.token,
    this.geofenceEvent,
  });
}
