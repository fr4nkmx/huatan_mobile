// To parse this JSON data, do
//
//     final cambioTramoModel = cambioTramoModelFromJson(jsonString);

import "dart:convert";

CambioTramoModel cambioTramoModelFromJson(String str) =>
    CambioTramoModel.fromJson(json.decode(str));

String cambioTramoModelToJson(CambioTramoModel data) =>
    json.encode(data.toJson());

class CambioTramoModel {
  int id;
  int estadoNotificacion;
  String ini;
  String fin;
  String origen;
  String destino;
  double destinoLat;
  double destinoLon;

  CambioTramoModel({
    this.id,
    this.estadoNotificacion,
    this.ini,
    this.fin,
    this.origen,
    this.destino,
    this.destinoLat,
    this.destinoLon,
  });

  factory CambioTramoModel.fromJson(Map<String, dynamic> json) =>
      CambioTramoModel(
        id: json["id"],
        estadoNotificacion: json["estadoNotificacion"],
        ini: json["ini"],
        fin: json["fin"],
        origen: json["origen"],
        destino: json["destino"],
        destinoLat: json["destinoLat"],
        destinoLon: json["destinoLon"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "estadoNotificacion": estadoNotificacion,
        "ini": ini,
        "fin": fin,
        "origen": origen,
        "destino": destino,
        "destinoLat": destinoLat,
        "destinoLon": destinoLon,
      };
}
