// To parse this JSON data, do
//
//     final asistenciaSemana = asistenciaSemanaFromJson(jsonString);

import 'dart:convert';

List<AsistenciaSemana> asistenciaSemanaFromJson(String str) =>
    List<AsistenciaSemana>.from(
        json.decode(str).map((x) => AsistenciaSemana.fromMap(x)));

String asistenciaSemanaToJson(List<AsistenciaSemana> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class AsistenciaSemana {
  int index;
  String diaSemana;
  String horaEntrada;
  String horaSalida;
  bool asistencia;
  bool retardo;
  bool falta;

  AsistenciaSemana({
    this.index,
    this.diaSemana,
    this.horaEntrada,
    this.horaSalida,
    this.asistencia,
    this.retardo,
    this.falta,
  });

  factory AsistenciaSemana.fromMap(Map<String, dynamic> json) =>
      AsistenciaSemana(
        index: json["index"],
        diaSemana: json["diaSemana"],
        horaEntrada: json["horaEntrada"],
        horaSalida: json["horaSalida"],
        asistencia: json["asistencia"],
        retardo: json["retardo"],
        falta: json["falta"],
      );

  Map<String, dynamic> toMap() => {
        "index": index,
        "diaSemana": diaSemana,
        "horaEntrada": horaEntrada,
        "horaSalida": horaSalida,
        "asistencia": asistencia,
        "retardo": retardo,
        "falta": falta,
      };
}
