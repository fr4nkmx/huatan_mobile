import 'package:attendance/src/models/cambio_tramo_model.dart';

class CambioTramoResponse {
  final CambioTramoModel result;
  final String error;
  final int statusCode;

  CambioTramoResponse(this.result, this.error, this.statusCode);

  CambioTramoResponse.fromJson(Map<String, dynamic> json, int statusCode)
      : result = CambioTramoModel.fromJson(json),
        statusCode = statusCode,
        error = "";

  CambioTramoResponse.withError(String error, int statusCode)
      : result = CambioTramoModel(),
        error = error,
        statusCode = statusCode;
}
