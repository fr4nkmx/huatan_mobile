import 'package:attendance/src/models/empleado_model.dart';

// RESPONSE FOR EMPLEADO ENTITY<
class EmpleadoResponse {
  final EmpleadoModel result;
  final String error;
  final int statusCode;

  EmpleadoResponse(this.result, this.error, this.statusCode);

  EmpleadoResponse.fromJson(Map<String, dynamic> json, int statusCode)
      : result = EmpleadoModel.fromJson(json),
        statusCode = statusCode,
        error = "";

  EmpleadoResponse.withError(String errorValue)
      : result = null,
        statusCode = 0,
        error = errorValue;
}
