import 'package:attendance/src/models/vacacion_model.dart';

class VacacionResponse {
  final VacacionModel result;
  final String error;
  final int statusCode;

  VacacionResponse(this.result, this.error, this.statusCode);

  VacacionResponse.fromJson(Map<String, dynamic> json, int statusCode)
      : result = VacacionModel.fromJson(json),
        statusCode = statusCode,
        error = "";

  VacacionResponse.withError(String error, int statusCode)
      : result = VacacionModel(),
        error = error,
        statusCode = statusCode;
}
