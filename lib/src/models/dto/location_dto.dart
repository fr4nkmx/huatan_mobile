class LocationDto {
  final int empleadoId;
  final String nomina;
  final String nombre;
  final String obra;
  final String puesto;
  final String telefono;
  final String deviceId;
  final double lat;
  final double lon;
  final int timestamp;
  final String zona;

  LocationDto({
    this.empleadoId,
    this.nomina,
    this.nombre,
    this.obra,
    this.puesto,
    this.telefono,
    this.deviceId,
    this.lat,
    this.lon,
    this.timestamp,
    this.zona,
  });

  LocationDto.fromJson(Map<String, dynamic> json)
      : empleadoId = json['empleadoId'],
        nomina = json['nomina'],
        nombre = json['nombre'],
        obra = json['obra'],
        puesto = json['puesto'],
        telefono = json['telefono'],
        deviceId = json['deviceId'],
        lat = json['lat'],
        lon = json['lon'],
        timestamp = json['timestamp'],
        zona = json['zona'];

  Map<String, dynamic> toJson() => {
        'empleadoId': empleadoId,
        'nomina': nomina,
        'nombre': nombre,
        'obra': obra,
        'puesto': puesto,
        'telefono': telefono,
        'deviceId': deviceId,
        'lat': lat,
        'lon': lon,
        'timestamp': timestamp,
        'zona': zona
      };
}
