// To parse this JSON data, do
//
//     final NotificationModel = notifcationModelFromJson(jsonString);

import 'dart:convert';

NotificationModel notifcationModelFromJson(String str) =>
    NotificationModel.fromJson(json.decode(str));

String notifcationModelToJson(NotificationModel data) =>
    json.encode(data.toJson());

class NotificationModel {
  int vacacionId;
  int cambioId;
  int incapacidadId;
  String estadoProceso;
  String estadoNotificacion;
  String tipoNotificacion;
  String ini;
  String fin;
  String lat;
  String lon;
  String map;
  String fecha;

  NotificationModel({
    this.vacacionId,
    this.cambioId,
    this.incapacidadId,
    this.estadoProceso,
    this.estadoNotificacion,
    this.tipoNotificacion,
    this.ini,
    this.fin,
    this.lat,
    this.lon,
    this.map,
    this.fecha,
  });

  factory NotificationModel.fromJson(Map<String, dynamic> json) =>
      NotificationModel(
        vacacionId: json["vacacionId"],
        cambioId: json["cambioId"],
        incapacidadId: json["incapacidadId"],
        estadoProceso: json["estadoProceso"],
        estadoNotificacion: json["estadoNotificacion"],
        tipoNotificacion: json["tipo_notificacion"],
        ini: json["ini"],
        fin: json["fin"],
        lat: json["lat"],
        lon: json["lon"],
        map: json["map"],
        fecha: json["fecha"],
      );

  Map<String, dynamic> toJson() => {
        "vacacionId": vacacionId,
        "cambioId": cambioId,
        "incapacidadId": incapacidadId,
        "estadoProceso": estadoProceso,
        "estadoNotificacion": estadoNotificacion,
        "tipo_notificacion": tipoNotificacion,
        "ini": ini,
        "fin": fin,
        "lat": lat,
        "lon": lon,
        "map": map,
        "fecha": fecha,
      };
}
