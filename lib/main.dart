import 'dart:async';

import 'package:attendance/src/bloc/bloc_provider.dart';
import 'package:attendance/src/pages/add_incapacidad.dart';
import 'package:attendance/src/pages/no_registrado_page.dart';
import 'package:attendance/src/services/push_notification_service.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

import 'package:attendance/src/pages/asistencia_page.dart';
import 'package:attendance/src/pages/cambios_page.dart';
import 'package:attendance/src/pages/home_page.dart';
import 'package:attendance/src/pages/incapacidad_page.dart';
import 'package:attendance/src/pages/login_page.dart';
import 'package:attendance/src/pages/otp_page.dart';
import 'package:attendance/src/pages/vacaciones_page.dart';
import 'package:attendance/src/services/geofence_service.dart';
import 'package:attendance/src/services/location_service.dart';
import 'package:attendance/src/services/local_notification_service.dart';

import 'package:attendance/src/utils/preferences.dart';
import 'package:flutter/material.dart';

import 'package:permission_handler/permission_handler.dart';

void main() async {
  Crashlytics.instance.enableInDevMode = false;
  FlutterError.onError = Crashlytics.instance.recordFlutterError;
  // await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  WidgetsFlutterBinding.ensureInitialized();

  // Permisions
  await PermissionHandler().requestPermissions([
    PermissionGroup.location,
    PermissionGroup.storage,
    PermissionGroup.camera
  ]);

  // User Preferences
  final prefs = new Preferences();
  await prefs.initPrefs();

  // Geofence
  final geofence = new GeofenceService();
  await geofence.initGeofence();

  // Tracking App
  final location = new LocationService();
  await location.init();

  // Notificaciones locales
  final localNotifications = new LocalNotificationService();
  await localNotifications.init();

  // Message Service
  final pushNotification = new PushNotificationService();
  pushNotification.initNotifications();

  // runApp(MyApp());
  runZoned<Future<void>>(() async {
    runApp(MyApp());
  }, onError: Crashlytics.instance.recordError);
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _prefs = Preferences();
  final _pushNotifications = PushNotificationService();
  final _geofenceService = GeofenceService();
  final _location = new LocationService();

  final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  @override
  void initState() {
    _pushNotifications.notificaciones.listen((data) {
      print("##### DATA NOTIFICACION MAIN ####");
      print(data);
      navigatorKey.currentState.pushNamed('visor', arguments: data);
    });

    super.initState();
    // _geofenceService.registerGeofence(
    //   'casa',
    //   19.395817,
    //   -99.133339,
    //   100.0,
    // );
    if (_prefs?.empleado != null) {
      _geofenceService.registerGeofences(_prefs.empleado.geocercaModel);      
    } else {
      _prefs?.clear();
    }
  }

  @override
  void dispose() {    
    super.dispose();    
    _location.stop();
  }

  @override
  Widget build(BuildContext context) {
    print('### prefs ###');
    print(_prefs?.signed);
    final iniPage = _prefs?.signed == true ? 'home' : 'login';

    // final iniPage = 'no_registrado';
    return BlocProvider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: new ThemeData(
          primaryColor: Color.fromRGBO(121, 159, 146, 1.0),
        ),
        title: 'Control Asistencia',
        initialRoute: iniPage,
        navigatorKey: navigatorKey,
        routes: {
          'home': (context) => HomePage(),
          'incapacidad': (context) => IncapacidadPage(),
          'cambios': (context) => CambiosPage(),
          'vacaciones': (context) => VacacionesPage(),
          'asistencia': (context) => AsistenciaPage(),
          'login': (context) => LoginPage(),
          'no_registrado': (context) => NoRegistradoPage(),
          'otp': (context) => OtpPage(),
          'add_incapacidad': (context) => AddIncapacidadPage()
        },
      ),
    );
  }
}
